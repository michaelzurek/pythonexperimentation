# Take the user input, and parse it for punctuation.
def puncKiller(words):
    # Establish a list of punctuation.
    puncList = [".","!",";",":",",","?"]
    # Parse through the list, and find instances of punctuation.
    # Then, replace the punctuation with spaces.
    # This works because multispaces are ignored by the split() command.
    for punc in range(len(puncList)):
        words = words.replace(puncList[punc]," ")
    theList = words.split()
    return theList

def main():
    usrInp = input("Please enter 1 or more words, separated by spaces. ")
    inpList = puncKiller(usrInp)
    print(inpList)
    print("There are",len(inpList),"words.")
    return

main()
