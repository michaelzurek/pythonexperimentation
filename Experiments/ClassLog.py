# ClassLog.py is a program that will:
    # Collect data on classes taken
    # Calculate GPA based on grades
    # Project final GPA based on course performance
    # Log all data in a file

# Time module is for the sleep() function
import time

def combine():
    # Ask user for filename of combined data
    # Ask user for the files they wish to combine in chronological order
    # Write data into newfile
    # Return to the menu
    menu()
    return

def view():
    # Ask user for the semester they want to load
    # Find and load <semester><year> based on user input
    # Ask user for what they wish to do
        # Modify
        # Print data
        # Find GPA
    # Return to the menu
    menu()
    return

# isFinished() is designed to be used as part of a while loop
# so that there need not be gross gotos in the case of bad input
def isFinished(done):
    finished = input("Finished? (Y/N) ")
    if finished.upper() == "Y":
        done = True
    elif finished.upper() == "N":
        pass
    else:
        print("ERROR: Invalid menu choice.")
        isFinished(done)
    return done

def record():
    # Ask user for the semester they wish to record data for
    semester = input("Enter the semester you wish to record for. Example: Fall2018\n")
    # Prepare and load a blank file 
    semFile = open(semester,'w')
    # Create a list for the class data
    classList = []
    # Until user is done, record any number of classes in a tuple
    done = False
    while not done:
        # Ask for class number
        classNum = input("Enter the class number. Ex: CSCI121 ")
        # Ask for credit hours
        classCredit = int(input("Enter the credit hours. Ex: 4 "))
        # Ask for final grade
        classGrade = input("Enter the final grade. Ex: A+ ")
        # Create tuple out of input
        classData = (classNum, classCredit, classGrade)
        # Push tuple into the list of classes
        classList.append(classData)
        # Ask user if finished inputting classes
        done = isFinished(done)
    # Write list of classes into file
    for item in range(len(classList)):
        print(classList[item],file=semFile)
    # Return to the menu
    menu()
    return

def menu():
    # Present the menu
        # Record semester class data
        # View semester class data
        # Combine semester data files 
        # Quit
        # Based on menu(), do the action requested
    print("(R)ecord semester data")
    print("(V)iew semester data")
    print("(C)ombine semester data")
    print("(Q)uit")
    menuChoice = input("Select a menu option. ")
    if menuChoice.upper() == ("R"):
        record()
    elif menuChoice.upper() == ("V"):
        view()
    elif menuChoice.upper() == ("C"):
        combine()
    elif menuChoice.upper() == ("Q"):
        return
    else:
        print("\nERROR: Invalid menu choice.\n")
        time.sleep(1.5)
        menu()
    return

def main():
    # Present the first instance of the menu
    menu()
    return

main()