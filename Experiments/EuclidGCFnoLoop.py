# Contains the Euclidean algorithm for finding the GCF between two numbers
def euclid(leftnum, rightnum):
    # Check if num1 is less than num2
    # If so, swap them so that num1 is bigger than num2
    # Pass num1 and num2 as leftnum and rightnum respectively
    if leftnum <= rightnum:
        temp = rightnum
        rightnum = leftnum
        leftnum = temp  
    # Check if rightnum is already 0.
    # If so, return the GCF, which is leftnum.
    # Otherwise, rerun euclid() with rightnum and the remainder of leftnum mod rightnum
    # This follows Euclid's algorithm.
    if rightnum == 0:
        return leftnum
    else:
        return euclid(rightnum, leftnum % rightnum)

# Asks user for two numbers to find the GCF of, then sends them to euclid()
def main():
    num1 = int(input("Please enter the first number. "))
    num2 = int(input("Please enter the second number. "))
    # Pass num1 and num2 as leftnum and rightnum respectively
    print(euclid(num1,num2), "is the GCF between",num1,"and",num2) # euclid() is run as part of this print statement. I'm not sure if this is bad practice.
    return

main()
