# Contains the Euclidean algorithm for finding the GCF between two numbers
def euclid(leftnum, rightnum):
    # Check if num1 is less than num2
    # If so, swap them so that num1 is bigger than num2
    # Pass num1 and num2 as leftnum and rightnum respectively
    if leftnum <= rightnum:
        temp = rightnum
        rightnum = leftnum
        leftnum = temp
    # While rightnum is not zero,
        # Set rightnum into a temporary variable
        # Set leftnum modulo rightnum into rightnum
        # Set leftnum to temporary variable
    # Then return leftnum
    # This follows Euclid's algorithm.
    while rightnum != 0:
        temp = rightnum
        rightnum = leftnum % rightnum
        leftnum = temp
    return leftnum

# Asks user for two numbers to find the GCF of, then sends them to euclid()
def main():
    num1 = int(input("Please enter the first number. "))
    # Check if input is positive. If not, quit with error.
    if num1 <= 0:
        print("Error: Input must not be a negative number or zero.")
        return
    num2 = int(input("Please enter the second number. "))
    # Check if input is positive. If not, quit with error.
    if num2 <= 0:
        print("Error: Input must not be a negative number or zero.")
        return
    print(euclid(num1,num2), "is the GCF between",num1,"and",num2) # euclid() is run as part of this print statement. I'm not sure if this is bad practice.
    return

main()
