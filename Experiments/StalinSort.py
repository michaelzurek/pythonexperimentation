# Algorithm
# Iterate through a series of numbers
    # If number is more than the next number
        # Remove that number from the list
    # Otherwise, move to the next number
# Print the final series, with the dissidents removed

def stalinSort(theList):
    length = len(theList)
    i = 1
    while i < length:
        if theList[i] < theList[i-1]:
            theList.pop(i)
            i = i - 1
            length = length - 1
        i = i + 1
    return theList
def main():
    sortInp = input("Please give me a list of any length to sort, separated by spaces. ")
    sortList = sortInp.split()
    for i in range(len(sortList)):
        sortList[i] = float(sortList[i])
    print(stalinSort(sortList))
    return

main()