import random

def numGen(numbers,maxNum,fileName):
    # Take numbers and maxLen from main() and use them to generate a file, containing 'numbers' numbers
    # of up to 'maxNum' value.

    # Create a new file
    numfile = open(fileName,'w')
    # print("Hello World",file=numfile)
    ## Loop
    for num in range(0,(maxNum)):
        print(str(random.randint(0,maxNum)) + ' ',file=numfile)
        



def main():
    # Take in the amount of numbers to generate, and the maximum number size
    # Send these to numGen()
    numbers = int(input("How many numbers should I generate? "))
    maxNum = int(input("How large should the numbers be able to get? "))
    fileName = input("What file should I place these numbers in? ")
    numGen(numbers,maxNum,fileName)
    return

main()