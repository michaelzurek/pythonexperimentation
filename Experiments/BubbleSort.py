def bubbleSort(theList):
    # Pass through the list and sort each number that needs to be sorted.
    # If the loop passes without any swaps, bail and return the sorted list.
    # Otherwise, keep swapping until the loop passes without any swaps.
    # New idea. Use only the "check" variable, instead of "check" and "swap"
    # "swap" should be unnecessary because if check = range(length-1), it completed
    # a pass with no swaps.
    

    length = len(theList) - 1
    # for 
    #     for i in range(length):
    #         if theList[i] <= theList[i+1]:
    #             check = check + 1
    #         else:
    #             temp = theList[i]
    #             theList[i] = theList[i+1]
    #             theList[i+1] = temp
    #             check = 0
    # else:
    #     return theList

    check = 0
    while check < length:
        for i in range(length):
            if theList[i] <= theList[i+1]:
                check = check + 1
            else:
                temp = theList[i]
                theList[i] = theList[i+1]
                theList[i+1] = temp
                check = 0
    else:
        return theList
    
        
def main():
    # The below is for implementing user-input. This is not implemented yet.
    sortInp = input("Please give me a list of any length to sort, separated by spaces. ")
    sortList = sortInp.split()
    for i in range(len(sortList)):
        sortList[i] = float(sortList[i])
    print(bubbleSort(sortList))
    return

main()
