
def stretchContrast(theValue,theMinimum,theMaximum):
    # Scale a value to a 256-color value
    newVal = (theValue - theMinimum) * (255/(theMaximum - theMinimum))
    newVal = int(newVal)
    return newVal

def main():
    # Ask for the minimum grayscale pixel value
    # Check if the value is greater than or equal to 0
        # And less than or equal to 255
        # Print an error and quit if it is not
    minimum = int(input("Give me a minimum grayscale pixel value: "))
    if not minimum >= 0 or not minimum <= 255:
        print("Error: Minimum is out of range (0-255). Quitting...")
        return
    # Ask for the maximum grayscale pixel value
    # Check if the value is greater than or equal to 0
        # And less than or equal to 255
        # Print an error and quit if it is not
    maximum = int(input("Give me a maximum grayscale pixel value: "))
    if not maximum >= 0 or not maximum <= 255:
        print("Error: Maximum is out of range (0-255). Quitting...")
        return
    # Check to ensure the minimum and maximum values are not equal
        # Print an error and quit if so
    if minimum >= maximum:
        print("Error: Minimum and maximum are equal or inverted to each other. Quitting...")
        return
    # Print the header
    print("Old\tNew")
    # Repeat the stretch for each value between the inputted minimum and maximum
        # and display it to the user.
    for theValue in range(minimum,(maximum+1)):
        print(theValue,"\t",stretchContrast(theValue,minimum,maximum))
    return

main()
