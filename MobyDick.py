def stringToWords(words):
    # Make the input string 
    words = words.lower()
    # Establish a list of punctuation.
    puncList = [".","!",";",":",",","?","--",'"',")","("]
    # Parse through the list, and find instances of punctuation.
    # Then, replace the punctuation with spaces.
    # This works because multispaces are ignored by the split() command.
    for punc in range(len(puncList)):
        words = words.replace(puncList[punc]," ")
    theList = words.split()
    return theList

def processWords(sanitizedList,mobyDict):
    
    for item in sanitizedList:
        # Create an if statement that checks if the current word is in the dictionary
        if item in mobyDict:
            # If true, use the word as a key and increase its occurance by 1
            mobyDict[item] = mobyDict[item] + 1
            # If false, use the word as a key to initialize the counting at 1
        else:
            mobyDict[item] = 1
    return mobyDict

def sortMobyDict(mobyDict):
    # Create a blank list
    newMoby = []
    
    # Create a list of all the keys, no sorting necessary.
    mobyDictKeys = list(mobyDict.keys())
    
    # Create a list of tuples, value 0 being the key,
        # and value 1 being the number of instances
    for item in mobyDictKeys:
        newMoby.append((item,mobyDict[item]))
    # Return the list sorted, based on the values, reversed.
    return sorted(newMoby,key=lambda theValue: theValue[1],reverse=True)


def main():
    # Prompt the user for a filename
    fileName = input("Give the name of the file you want to process. ")
    # Open the file for input
    theFile = open(fileName)
    # Create an empty dictionary for the counting
    mobyDict = {}
    # Use a loop to process for each line in the input
    for line in theFile:
        # Remove \n
        line = line.replace('\n','')
        # Call stringToWords() and store in WordList
        wordList = stringToWords(line)
        # Call processWords passing in wordList
        mobyDict = processWords(wordList,mobyDict)
    # Add code to sort the keys alphabetically in the dictionary that was returned by processWords()
    # Use a loop to print out each key and the stored count from the dictionary on a separate line
    newSortedMoby = sortMobyDict(mobyDict)
    for item in newSortedMoby:
        print(item[0],"\t",item[1])
    return

main()
