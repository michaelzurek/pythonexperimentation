
def main():
    print("This program calculates loans.")

    principal = float(input("Please enter the inital principal: "))
    interest = float(input("Enter the interest rate: "))
    term = int(input("Enter the loan term in months: "))
    payment = float(input("Please enter your monthly payment: "))

    interest = (interest/100) # For allowing input interest to be user-friendly. Ex: 1.96
    interest = (interest/12) # Convert annual percentage to monthly percentage
    interest = (1+interest) # Convert interest to a multiplication-ready value
    # print(interest) - This is a debugging line. Not necessary.
    
    for i in range(term):
        principal = principal * interest 
        principal = (principal-payment)

    print("The final balance after",term, "months is",principal)

    principal = (principal/payment) * -1

    print("You saved",principal,"months worth of payments!")

main()
