def main():

    # Program for computing the value of an investment over time
    # Accounts for removal of money from the principal
    
    print("I will calculate the future value")
    print("of a 10-year investment.")

    principal = float(input("Please enter the inital principal: "))
    apr = float(input("Please enter the annual interest rate: "))

    apr = (apr/100) # This allows the input apr to be a human readable percentage, and not a range of 0 to 1.0

    scholarship = float(input("Please enter the scholarship amount: "))
    
    for i in range(10):
        principal = principal * (1 + apr)
        principal = (principal-scholarship)

    print("The value in 10 years will be:", principal)

main()
