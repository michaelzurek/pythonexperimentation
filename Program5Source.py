# Prime Number Checker
# Algorithm
## PrimeCheck (is prime? true or false)
### Is the number even?
#### Yes - Return False
### For every odd number from 3 to sqrt(inputVar)
#### Check if divides equally
##### Yes - Bail out with false
### Send back true

import math

def main():

    primeInput = int(input("Please enter an integer to prime check. "))

    if primeInput % 2 == 0:
        print(primeInput,"isn't a prime number. It is divisible by 2. ")

    else:
        primeInputSqrt = math.floor(math.sqrt(primeInput)) # Run the floor here to save compute cycles? I'll ask Prof. Smith if this helps
        count = 2 # count is set to 2 because all primes are divisble by 1, so it does not need to be accounted for.

        if primeInputSqrt ** 2 == primeInput:
            print(primeInput,"isn't a prime number. It has an integer square root.")
            
        else:
            while count <= primeInputSqrt:
                temp = primeInput % count
                if temp == 0:
                    print(primeInput,"isn't a prime number. It is divisble by",count)
                    return
                
                count = count + 1
                
            print("This number is a prime number!")
            return
main()
