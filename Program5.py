import math

# Define the function isPrime with a single paramater called candidate
def isPrime(candidate):

    # Return False if the candidate is even
    if candidate % 2 == 0:
        return False
    # Otherwise the candidate is odd
    else:
        # Calculate the square root of the candidate using the math
        # library sqrt function. Store this in a variable called
        # root. Make sure the result in root is an integer
        root = int(math.sqrt(candidate))
        # Run a loop over the numbers from 3 to root+1 stepping by 2
            # If the loop variable evenly divides the candidate, return
            # False
        for count in range(3,root+1,2):
            # temp = candidate % count
            if (candidate % count == 0):
                return False

        # Execution will only reach here if the loop didn't find a
        # number that evenly divides the candidate, so return True
        return True
# Define a function called main() with no parameters
def main():
    # Ask the user for their number, store it in theNum
        # Make sure it is an integer
    theNum = int(input("Please enter an integer to prime check. "))
    # If isPrime(), when called passing in theNum, returns True, print
    # that the number is prime.
    # Otherwise print that the number is not prime.
    isPrime(theNum)
    if (isPrime(theNum)):
        print("The number",theNum,"is prime.")
    else:
        print("The number",theNum,"is not prime.")
# Call main()
main()
